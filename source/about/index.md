---
title: about
date: 2017-02-06 17:56:38
---

[シンデレラガールズ総合データベース構築プロジェクト hinapedia](https://gitlab.com/tottokotkd/hinapedia/boards) に関する情報を掲載しています。

* [hinapediaについて]()
* [ユニット収録の判断基準](https://gitlab.com/tottokotkd/hinapedia/issues/9)

## author
* [tottokotkd](http://blog.tottokotkd.com/)